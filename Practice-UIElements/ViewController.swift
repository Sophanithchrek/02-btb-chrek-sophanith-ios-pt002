//
//  ViewController.swift
//  Practice-UIElements
//
//  Created by SOPHANITH CHREK on 11/13/20.
//

import UIKit

class ViewController: UIViewController {

    // Step2: Create the properties as PersonalDelegate
    var personalDelegate: PersonalDelegate?
//    var personalInfoDelegate: PersonalInfoDelegate?
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set first focus of keyborad to txtUsername
        txtUsername.becomeFirstResponder()
        
        // Change keyborad type of textfield
        txtEmail.keyboardType = .emailAddress
        txtPhoneNumber.returnKeyType = .done
        
        // Assign all textfield to all UITextFieldDelegate event
        txtUsername.delegate = self
        txtAddress.delegate = self
        txtEmail.delegate = self
        txtPhoneNumber.delegate = self
        
    }
    
    // Function to send user's data to next ScreenViewController
    @IBAction func btnSendPressed(_ sender: UIButton) {
        
        let username = txtUsername.text
        let address = txtAddress.text
        let email = txtEmail.text
        let phoneNumber = txtPhoneNumber.text
        
        // Declare variable to get image from UIImageView
        let image = imageView.image!
        
        // Create object from SecondViewController class, purpose to open new form
        let secondForm = storyboard?.instantiateViewController(identifier: "secondForm") as! SecondViewController
        
        // Step4: Set delegate to SecondViewController
        self.personalDelegate = secondForm
        
        // Step5: Call the properties of class that have properties of PersonalDelegate and provide data to it, purpose to make both SecondViewController have the same data from ViewController
        personalDelegate?.sendData(username: username ?? "", address: address ?? "", email: email ?? "", phoneNumber: phoneNumber ?? "", image: image)
        
        // Clear PersonalInfo before open SecondViewController
        txtUsername.text = ""
        txtAddress.text = ""
        txtEmail.text = ""
        txtPhoneNumber.text = ""
        
        // Set fullScreen to SecondViewController form
        secondForm.modalPresentationStyle = .fullScreen
        
        // Open other ViewController, using present() method
        present(secondForm, animated: true, completion: nil)
        
    }

}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Try to find next responder
        if let nextElement = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextElement.becomeFirstResponder()
        } else {
            // Not found, so remove keyborad.
            textField.resignFirstResponder()
        }
        
        // Return false to close Keyboard
        return false
    }
}
