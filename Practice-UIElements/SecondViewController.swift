//
//  SecondViewController.swift
//  Practice-UIElements
//
//  Created by SOPHANITH CHREK on 11/13/20.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    
    var info: String?
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Disable editing of text in UITextView
        textView.isEditable = false
        
        // Add data into UITextView
        textView.text = info
        
        // Add image to UIImageView
        imageView.image = image
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        // Call dismiss() method to dispose form
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// Step3: Make SecondViewControoler conform to PersonalDelegate and Override Method of PersonalDelegate
extension SecondViewController: PersonalDelegate {
    func sendData(username: String, address: String, email: String, phoneNumber: String, image: UIImage) {
        
        // Set Personal Info to variable to store info
        info = "Name: \(username) \nAddress: \(address) \nEmail: \(email) \nPhone Number: \(phoneNumber)"
        
        // Set image to get image
        self.image = image
    }
}
