//
//  PersonalInfoDelegate.swift
//  Practice-UIElements
//
//  Created by SOPHANITH CHREK on 11/13/20.
//

import UIKit

// Step1: Create protocol
protocol PersonalDelegate {
    func sendData(username: String, address: String, email: String, phoneNumber: String, image: UIImage)
}
